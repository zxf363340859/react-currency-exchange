import React, {Component} from 'react';
import CurrencyInput from "./CurrencyInput";

class CurrencyTranslator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            USD: '',
            CNY: ''
        }
        this.handleChangeUSD = this.handleChangeUSD.bind(this);
        this.handleChangeCNY = this.handleChangeCNY.bind(this);
    }

    handleChangeUSD(money) {
        this.setState({
            CNY: '' + money,
            USD: '' + money / 6
        });
    }

    handleChangeCNY(money) {
        this.setState({
            CNY: '' + money * 6,
            USD: '' + money
        });
    }

  render() {
    return (
      <div>
        <CurrencyInput currency="USD" money={this.state.USD} onChange = {this.handleChangeCNY}/>
        <CurrencyInput currency="CNY"  money={this.state.CNY}  onChange = {this.handleChangeUSD}/>
      </div>
    )
  }

}

export default CurrencyTranslator;
