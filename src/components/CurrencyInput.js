import React, {Component} from 'react';

class CurrencyInput extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange (event) {
      this.props.onChange(event.target.value);
  }

  render() {
    return(
      <section>
        <label>{this.props.currency}</label>
        <input
          type="text"
          onChange={this.handleChange}
          value={
            this.props.money
          }
        />
      </section>
    )
  }
}

export default CurrencyInput;
