import React, {Component} from 'react';
import './App.less';
import CurrencyTranslator from "./components/CurrencyTranslator";

class App extends Component{

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='App'>
        <CurrencyTranslator/>
      </div>
    );
  }
}

export default App;
